## 跨平台脚本
该目录下存放可跨平台使用的脚本。

### 脚本列表
 * [`pyclear`](/cross-platform/pyclear/README.md) 清理 Python 缓存文件（`__pycache__`）
