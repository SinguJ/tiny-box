## PyClear
### 介绍
`PyClear` 是一个命令行脚本，用来清理 Python 脚本执行过后所留下的 `__pycache__` 文件夹，它会扫描当前目录（或指定目录）下的每一个文件夹，查找并删除所有名为 `__pycache__` 的文件夹，及其下的所有文件。

### 安装
本脚本依赖 Python 运行环境，如果您还没有安装，请[点击这里](https://www.liaoxuefeng.com/wiki/1016959663602400/1016959856222624)。

#### Linux & MacOS
```bash
curl -o /usr/local/bin/pyclear "https://gitee.com/SinguJ/tiny-box/raw/master/cross-platform/pyclear/pyclear"
chmod a+x /usr/local/bin/pyclear
```

### 使用
```bash
# 查看帮助
pyclear -h

# 清理当前目录
cd <path>
pyclear

# 清理指定目录
pyclear <path1> <path2> ...

# 显示清理日志
pyclear -v
## or
pyclear -v <path1> <path2> ...
```