## DsClear
### 介绍
`DsClear` 是一个命令行脚本，用来清理 MacOS 下的 `.DS_Store` 文件，它会扫描当前目录（或指定目录）下的每一个文件，查找并删除所有名为 `.DS_Store` 的文件。

### 安装
本脚本依赖 Python 运行环境，如果您还没有安装，请[点击这里](https://www.liaoxuefeng.com/wiki/1016959663602400/1016959856222624)。
```bash
curl -o /usr/local/bin/dsclear "https://gitee.com/SinguJ/tiny-box/raw/master/macos/dsclear/dsclear"
chmod a+x /usr/local/bin/dsclear
```

### 使用
```bash
# 查看帮助
dsclear -h

# 清理当前目录
cd <path>
dsclear

# 清理指定目录
dsclear <path1> <path2> ...

# 显示清理日志
dsclear -v
## or
dsclear -v <path1> <path2> ...
```