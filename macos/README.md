## MacOS 系统专用脚本
该目录存放 MacOS 系统下的专用脚本

### 脚本列表
 * [`dnsleep`](/macos/dnsleep/README.md) 屏蔽 MacOS 系统自动休眠功能
 * [`dsclear`](/macos/dsclear/README.md) 清理 MacOS 系统中的 `.DS_Store` 文件
 * [`pbpwd`](/macos/pbpwd/README.md) 将当前目录的路径复制到剪贴板
