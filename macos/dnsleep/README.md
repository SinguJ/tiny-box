## DoNotSleep
该脚本可以有效屏蔽 MacOS 系统自动息屏功能，本着「运行即生效，关闭即解除」的理念，不会影响系统设置，只需要您在控制台挂起该程序即可。

### 安装
本脚本使用 ObjectiveC 编写，并直接编译成二进制程序，在 MacOS 系统上运行，不需要安装任何依赖环境。
```bash
curl -o /usr/local/bin/dnsleep "https://gitee.com/SinguJ/tiny-box/raw/master/macos/dnsleep/dist/dnsleep"
chmod a+x /usr/local/bin/dnsleep
```

### 使用
```bash
# 开启
## 开启后，该程序会一直运行，直到您手动关闭它
dnsleep

# 关闭
## 按组合键 [Ctrl + C]
```