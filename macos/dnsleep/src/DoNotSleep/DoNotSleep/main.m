//
//  main.m
//  DoNotSleep
//
//  Created by Singu on 2020/02/11.
//  Copyright © 2020 Alinado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IOKit/pwr_mgt/IOPMLib.h>

IOPMAssertionID assertionID;

enum UserSignal {
    Wait, Stop
} userSignal;

void signalHandler(int signum) {
    userSignal = Stop;
}

void waitCommand() {
    userSignal = Wait;
    
    signal(SIGINT, signalHandler);
    
    while (true) {
        if (userSignal == Stop)
            break;
        else
            sleep(1);
    }
}

int main(int argc, const char * argv[]) {
    // 禁止休眠
    if (IOPMAssertionCreateWithName(kIOPMAssertionTypeNoDisplaySleep, kIOPMAssertionLevelOn, CFSTR("DoNotSleep 禁止系统休眠"), &assertionID) == kIOReturnSuccess) {
        printf("本程序禁止系统休眠，请用「Ctrl+C」重新启用...\n");
        fflush(stdout);
        // 等待用户命令
        waitCommand();
        // 释放
        IOPMAssertionRelease(assertionID);
        printf("\n已再次启用系统休眠，程序退出！\n");
        fflush(stdout);
    }
    return 0;
}
