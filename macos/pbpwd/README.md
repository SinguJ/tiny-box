## pbpwd
### 介绍
`pbpwd` 是一个命令行脚本，它能将当前所在目录的路径复制到剪贴板中。

### 安装
本脚本是一个 Bash Shell 脚本，在 MacOS 系统上运行，不需要安装任何依赖环境。
```bash
curl -o /usr/local/bin/pbpwd "https://gitee.com/SinguJ/tiny-box/raw/master/macos/pbpwd/pbpwd"
chmod a+x /usr/local/bin/pbpwd
```

### 使用
```bash
# 获取当前目录
pbpwd
```