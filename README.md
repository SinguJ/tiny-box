# TinyBox
TinyBox 提供了各种常用的脚本，每个脚本都是完全独立的，您可以单独下载使用。

## 脚本列表
以下列表中，点击脚本名称即可查看脚本的详细说明。

### 跨平台脚本
 * [`pyclear`](/cross-platform/pyclear/README.md) 清理 Python 缓存文件（`__pycache__`）

### Linux 专用脚本

### MacOS 专用脚本
 * [`dnsleep`](/macos/dnsleep/README.md) 屏蔽 MacOS 系统自动休眠功能
 * [`dsclear`](/macos/dsclear/README.md) 清理 MacOS 系统中的 `.DS_Store` 文件
 * [`pbpwd`](/macos/pbpwd/README.md) 将当前目录的路径复制到剪贴板
 
### Windows 专用脚本
